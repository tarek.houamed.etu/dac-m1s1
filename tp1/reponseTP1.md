# Réponses questions

### Communication entre les machines sans firewall


#### Sur le client:
Commandes utisées:
`sudo nc <ip du serveur> <port>` j'établis une connexion avec le serveur pour lui envoyer des messages

![aperçu client](/res/tenta_sans_firewall.png)

#### Sur le serveur:
Commandes utilisées:
`tmux` afin de pouvoir observer les paquets d'un côté et de l'autre les messages reçu

`sudo nc -l 50` j'écoute sur le port 50
`tcpdump port 50` je capture les communications sur le port 50

 ![aperçu serveur](/res/tcpdump_sans_firewall.png)


### Communication entre les machines avec firewall

#### Sur le client:
Je fais la même chose.
![aperçu client fire](/res/2eme_tenta_firewall.png)

#### Sur le serveur:
Commandes utilisées:
`ufw enable` j'active un firewall grâce à ufw .

puis je fais la même chose en observant les communications.
![aperçu serveur fire](/res/tcpdump_firewall.png)

On observe que le serveur ne reçoit pas le "Hello World" transmis par le client.
En regardant de plus près le tcpdump sur le serveur on voit que le client essaye d'établir une connexion avec le serveur ([S] FLAG).
Mais que rien n'aboutit car le serveur ignore en ne répondant pas.
