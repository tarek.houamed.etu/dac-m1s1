terraform {
required_version = ">= 0.13.4"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.35.0"
    }
  }
}
resource "openstack_compute_keypair_v2" "test-keypair" {
  name       = "mykey"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCgi5rfnRdmjftKryvNcwA7q607dQgYuHGaL4vRLGadrm9Tk1iE2Bin3wkFaRVRcqht08paapKDO2G6GlWuS3n8yzJF30e0UbdU25+EAD/bzCQWkFFtJiuo6Ukw04JieC/XjrOcUtJLiXwbYxoWymjS/74nLr3QBf4wdB0ZDONzbAXjiX9VbUewLBANAL6ia+6QDvmJqPql6mE/u/O5Evjxah8H/jJV75286zbJVxl/0fNVsRV0tFpZzfQEgFAYtK97CdSuiJpJZ4+OEPCwU442qZiJvWAWRo0TvRVJ1g4I9ByKj5cbOM/UMXNOuT/KUP0tmA4jPC4Y4zHwYkcjngBP"
}



resource "openstack_compute_instance_v2" "berkat" {
  count="3"
  name            = "basic"
  image_id        = "bf420b24-7df0-485f-ae29-1f778c3d1df4"
  flavor_id       = "689721ab-4479-4406-b133-ab9db72b7910"
  key_pair        = "mykey"
  security_groups = ["default"]

  metadata = {
    this = "that"
  }

  network {
    name = "prive"
  }
}